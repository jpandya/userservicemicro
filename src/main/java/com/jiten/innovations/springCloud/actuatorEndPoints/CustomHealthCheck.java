package com.jiten.innovations.springCloud.actuatorEndPoints;

import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.HealthIndicator;
import org.springframework.stereotype.Component;

@Component
public class CustomHealthCheck implements HealthIndicator {

	private int errorCode = 0;
	
	
	@Override
	public Health health() {
		System.out.println("Health Check Performed, Error Code is - " + errorCode);
		if(errorCode > 2 && errorCode < 50) {
			errorCode++;
			return Health.down().withDetail("Custom Error Code", errorCode).build();
		}
		errorCode++;
		return Health.up().build();
	}

}
