package com.jiten.innovations.springCloud.UserServiceMicro;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.ComponentScan;

@EnableEurekaClient
@ComponentScan(basePackages = {
		"com.jiten.innovations.springCloud.controller" ,"com.jiten.innovations.springCloud.actuatorEndPoints" })
@SpringBootApplication
public class UserServiceMicroApplication {

	public static void main(String[] args) {
		SpringApplication.run(UserServiceMicroApplication.class, args);
		//System.out.println("JITEN >> " + System.getProperty("profiles"));
	}

}
